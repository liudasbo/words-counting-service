package com.task.wordcountingservice.controller;

import com.task.wordcountingservice.exception.ErrorType;
import com.task.wordcountingservice.model.ContentHolder;
import com.task.wordcountingservice.runner.FileProcessorRunner;
import com.task.wordcountingservice.serivce.ContentProcessingService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
class FileUploadControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    FileProcessorRunner fileProcessorRunner;

    @MockBean
    ContentHolder contentHolder;

    @MockBean
    ContentProcessingService contentProcessingService;

    private static final String SERVICE_BASE_URL = "http://localhost:8080/upload";

    @Test
    void uploadFile_shouldReturnExpectedResponseIfFileAttached() throws Exception {
        MockMultipartFile mockMultipartFile = new MockMultipartFile("files","fileName",
                "text/plain", "test data".getBytes());
        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.multipart(SERVICE_BASE_URL)
                .file(mockMultipartFile);
        mockRequest.contentType(MediaType.MULTIPART_FORM_DATA_VALUE);
        mockRequest.accept(MediaType.APPLICATION_JSON);

        ResultActions resultActions =  mockMvc.perform(mockRequest);
        resultActions.andExpect(status().isOk());
    }

    @Test
    void uploadFile_shouldReturnExpectedResponseIfNoIncorrectMethodUsed() throws Exception {
        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.get(SERVICE_BASE_URL);
        mockRequest.contentType(MediaType.MULTIPART_FORM_DATA_VALUE);
        mockRequest.accept(MediaType.APPLICATION_JSON);
        ResultActions resultActions =  mockMvc.perform(mockRequest);
        resultActions.andExpect(status().isMethodNotAllowed());
    }

    @Test
    void uploadFile_shouldReturnExpectedResponseIfUnsupportedMediaType() throws Exception {
        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post(SERVICE_BASE_URL);
        mockRequest.contentType(MediaType.APPLICATION_JSON);
        mockRequest.accept(MediaType.APPLICATION_JSON);
        ResultActions resultActions =  mockMvc.perform(mockRequest);
        resultActions.andExpect(status().isUnsupportedMediaType())
                .andExpect(jsonPath("$.code").value(ErrorType.CONTENT_TYPE_NOT_SUPPORTED_ERROR.code));
    }
}