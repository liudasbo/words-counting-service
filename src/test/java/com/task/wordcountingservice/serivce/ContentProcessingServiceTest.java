package com.task.wordcountingservice.serivce;

import com.task.wordcountingservice.config.FileChunksConfig;
import com.task.wordcountingservice.model.ContentHolder;
import com.task.wordcountingservice.model.FileChunk;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ContentProcessingServiceTest {

    @Mock
    ContentHolder contentHolder;

    @Mock
    FileChunksConfig fileChunksConfig;

    ContentProcessingService contentProcessingService;

    public static final String FIRST_FILE_NAME = "firstFile.txt";
    public static final String FIRST_REGEX = "^[-a-gA-G].*";
    public static final String SECOND_FILE_NAME = "secondFile.txt";
    public static final String SECOND_REGEX = "^[-h-nH-N].*";
    public static final String INVALID_FILE_NAME = "invalidFile.txt";
    public static final String INVALID_REGEX = "|";

    @BeforeEach
    void setup() {
        contentProcessingService = new ContentProcessingService(contentHolder, fileChunksConfig);
    }

    @Test
    void countDifferentWords_shouldCorrectlyCountWords() {
        Map<String, Integer> wordsWithCounts = new ConcurrentSkipListMap<>();
        ArrayList<String> uploadedWordsList = new ArrayList<>(Arrays.asList(
                "Test", "Test", "Test", "Bot", "Call", "Lorem", "ipsum"));
        when(contentHolder.getWordsWithCount()).thenReturn(wordsWithCounts);
        contentProcessingService.countDifferentWords(uploadedWordsList);
        Assertions.assertEquals(5, wordsWithCounts.size());
        Assertions.assertEquals(3, wordsWithCounts.get("test"));
    }

    @Test
    void splitWordsToSeparateCollections_shouldCorrectlySplitToChunks() {
        Map<String, Integer> wordsWithCounts = new ConcurrentSkipListMap<>();
        wordsWithCounts.put("arcade", 3);
        wordsWithCounts.put("bluffing", 2);
        wordsWithCounts.put("xamarin", 1);

        Map<String, FileChunk> fileChunks = new LinkedHashMap<>();
        fileChunks.put(FIRST_FILE_NAME, new FileChunk(FIRST_REGEX));
        fileChunks.put(SECOND_FILE_NAME, new FileChunk(SECOND_REGEX));

        when(fileChunksConfig.getFileChunks()).thenReturn(fileChunks);
        Map<String, LinkedHashMap<String, Integer>> result =
                contentProcessingService.splitWordsToSeparateCollections(wordsWithCounts);
        Assertions.assertEquals(2, result.get(FIRST_FILE_NAME).size());
        Assertions.assertEquals(0, result.get(SECOND_FILE_NAME).size());
    }

    @Test
    void splitWordsToSeparateCollections_shouldIgnoreIncorrectRegex() {
        Map<String, Integer> wordsWithCounts = new ConcurrentSkipListMap<>();
        wordsWithCounts.put("arcade", 3);
        wordsWithCounts.put("bluffing", 2);
        wordsWithCounts.put("xamarin", 1);

        Map<String, FileChunk> fileChunks = new LinkedHashMap<>();
        fileChunks.put(INVALID_FILE_NAME, new FileChunk(INVALID_REGEX));
        fileChunks.put(FIRST_FILE_NAME, new FileChunk(FIRST_REGEX));

        when(fileChunksConfig.getFileChunks()).thenReturn(fileChunks);
        Map<String, LinkedHashMap<String, Integer>> result =
                contentProcessingService.splitWordsToSeparateCollections(wordsWithCounts);
        Assertions.assertEquals(2, result.get(FIRST_FILE_NAME).size());
        Assertions.assertEquals(0, result.get(INVALID_FILE_NAME).size());
    }
}