package com.task.wordcountingservice.serivce;

import com.task.wordcountingservice.model.ContentHolder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;

import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class FileProcessorServiceTest {

    @Mock
    ContentHolder contentHolder;

    FileProcessorService fileProcessorService;

    private static final String UPLOADED_FILE_CONTENT = "Random file content";

    @BeforeEach
    void setup() {
        fileProcessorService = new FileProcessorService(contentHolder);
    }

    @Test
    void readFile_shouldSkipReadingFileIfItIsEmpty() {
        MultipartFile mockedMultipartFile = new MockMultipartFile("mockedFile.txt", new byte[]{});
        fileProcessorService.readFile(mockedMultipartFile);
        verify(contentHolder, never()).getUploadedWords();
    }

    @Test
    void readFile_shouldSuccesfullyReadFilesAndStoreToContentHolder() {
        ArrayList<String> testList = new ArrayList<>();
        MultipartFile mockedMultipartFile1 = new MockMultipartFile("mockedFile.txt",
                UPLOADED_FILE_CONTENT.getBytes());
        MultipartFile mockedMultipartFile2 = new MockMultipartFile("mockedFile.txt",
                UPLOADED_FILE_CONTENT.getBytes());
        when(contentHolder.getUploadedWords()).thenReturn(testList);
        fileProcessorService.readFile(mockedMultipartFile1);
        fileProcessorService.readFile(mockedMultipartFile2);
        Assertions.assertEquals(testList.size(), UPLOADED_FILE_CONTENT.split("\\W+").length * 2);
        verify(contentHolder, Mockito.times(2)).getUploadedWords();
    }
}