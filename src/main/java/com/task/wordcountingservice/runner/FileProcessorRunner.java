package com.task.wordcountingservice.runner;

import com.task.wordcountingservice.serivce.FileProcessorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ThreadPoolExecutor;

@Component
@Slf4j
public class FileProcessorRunner {

    private final FileProcessorService fileProcessorService;

    @Autowired
    public FileProcessorRunner(FileProcessorService fileProcessorService) {
        this.fileProcessorService = fileProcessorService;
    }

    /**
     * ThreadPoolExecutor object with configured size pool
     */
    ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);


    /**
     * Submit read file operation to threads for each passed
     * {@link org.springframework.web.multipart.MultipartFile} file as an argument
     *
     * @param files the files
     */
    public void processFiles(List<MultipartFile> files) {
        files.forEach(file -> {
                   FutureTask<?> futureTask =
                           (FutureTask<?>) executor.submit(() -> fileProcessorService.readFile(file));
                   futureTask.run();
        });
    }

    /**
     * Submit write to file operation to threads for each passed fileChunk as an argument
     *
     * @param fileChunks the file chunks
     */
    public void writeToFiles(Map<String, LinkedHashMap<String, Integer>> fileChunks) {
        fileChunks.forEach((fileName, fileContent) -> {
            FutureTask<?> futureTask = (FutureTask<?>) executor.submit(() ->
                    fileProcessorService.writeToFile(fileName, fileContent));
            futureTask.run();
        });
    }
}

