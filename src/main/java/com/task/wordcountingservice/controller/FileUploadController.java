package com.task.wordcountingservice.controller;

import com.task.wordcountingservice.model.ContentHolder;
import com.task.wordcountingservice.runner.FileProcessorRunner;
import com.task.wordcountingservice.serivce.ContentProcessingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
public class FileUploadController {

    private final FileProcessorRunner fileProcessorRunner;
    private final ContentHolder contentHolder;
    private final ContentProcessingService contentProcessingService;

    @Autowired
    public FileUploadController(FileProcessorRunner fileProcessorRunner,
                                ContentHolder contentHolder,
                                ContentProcessingService contentProcessingService) {
        this.fileProcessorRunner = fileProcessorRunner;
        this.contentHolder = contentHolder;
        this.contentProcessingService = contentProcessingService;
    }

    @PostMapping(value = "/upload",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity uploadFile(@RequestParam("files") List<MultipartFile> uploadedFiles) {
        if(uploadedFiles.isEmpty()) {
            return new ResponseEntity("No files have been attached", HttpStatus.BAD_REQUEST);
        }
        fileProcessorRunner.processFiles(uploadedFiles);
        contentProcessingService.countDifferentWords(contentHolder.getUploadedWords());
        Map<String, LinkedHashMap<String, Integer>> chunkedWords  =
                contentProcessingService.splitWordsToSeparateCollections(contentHolder.getWordsWithCount());
        fileProcessorRunner.writeToFiles(chunkedWords);
        return new ResponseEntity(contentHolder.getWordsWithCount(), HttpStatus.OK);
    }
}
