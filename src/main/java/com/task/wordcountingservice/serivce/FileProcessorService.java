package com.task.wordcountingservice.serivce;

import com.task.wordcountingservice.model.ContentHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Map;

/**
 * The type File processor service.
 */
@Service
@Slf4j
public class FileProcessorService {

    @Value("${wordcountingservice.filesPath}")
    private String filePath;

    private final ContentHolder contentHolder;

    @Autowired
    public FileProcessorService(ContentHolder contentHolder) {
        this.contentHolder = contentHolder;
    }

    /**
     * Read {@link org.springframework.web.multipart.MultipartFile} split all
     * content by words and add all read content to
     * {@link java.util.concurrent.CopyOnWriteArrayList}
     *
     * @param multipartFile the multipart file
     */
    public void readFile(MultipartFile multipartFile) {
        if (!multipartFile.isEmpty()) {
            byte[] bytes = new byte[0];
            try {
                bytes = multipartFile.getBytes();
            } catch (IOException ex) {
                log.error(ex.getMessage(), ex.getCause());
            }
            String fileData = new String(bytes);
            String[] wordsArray = fileData.split("\\W+");
            contentHolder.getUploadedWords().addAll(Arrays.asList(wordsArray));
        }
    }

    /**
     * Write content to the file in the configured location
     *
     * @param fileName    the file name
     * @param fileContent the file content
     */
    public void writeToFile(String fileName, Map<String, Integer> fileContent) {
        Path fileLoc = Paths.get(filePath + fileName);
        try (BufferedWriter writer = Files.newBufferedWriter(fileLoc, StandardCharsets.UTF_8)) {
            fileContent.forEach((key, value) -> {
                try {
                    writer.write(key + " " + value);
                    writer.newLine();
                } catch (IOException ex) {
                    log.error(ex.getMessage(), ex.getCause());
                }
            });
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex.getCause());
        }
    }
}