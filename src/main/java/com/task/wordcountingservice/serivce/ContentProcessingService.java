package com.task.wordcountingservice.serivce;

import com.task.wordcountingservice.config.FileChunksConfig;
import com.task.wordcountingservice.model.ContentHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * The type Content processing service.
 */
@Service
public class ContentProcessingService {

    private final ContentHolder contentHolder;
    private final FileChunksConfig fileChunksConfig;

    @Autowired
    public ContentProcessingService(ContentHolder contentHolder, FileChunksConfig fileChunksConfig) {
        this.contentHolder = contentHolder;
        this.fileChunksConfig = fileChunksConfig;

    }

    /**
     * Count and sort different words from a given list and put
     * the result to wordsWithCount {@link java.util.LinkedHashMap}
     *
     * @param fileContent the file content
     */
    public void countDifferentWords(List<String> fileContent) {
        Map<String, Integer> result = fileContent.stream()
                .map(String::toLowerCase)
                .sorted()
                .collect(Collectors.toMap(Function.identity(), element -> 1, (oldValue, newValue) -> oldValue + 1,
                        LinkedHashMap::new));
        contentHolder.getWordsWithCount().putAll(result);
    }

    /**
     * Split words collection to separate collections by given patterns
     *
     * @param sortedWordsWithOccurrences the sorted words with occurrences
     * @return Map with the fileName as a key and {@link java.util.LinkedHashMap} containing word and count
     * as a value
     */
    public Map<String, LinkedHashMap<String, Integer>> splitWordsToSeparateCollections
            (Map<String, Integer> sortedWordsWithOccurrences) {

        Map<String, LinkedHashMap<String, Integer>> wordsChunksByRegexes = new LinkedHashMap<>();

        fileChunksConfig.getFileChunks().entrySet().stream()
                .forEach(stringFileChunkEntry ->
                    wordsChunksByRegexes.put(stringFileChunkEntry.getKey(), splitToMapByPattern(sortedWordsWithOccurrences,
                            stringFileChunkEntry.getValue().getRegexPattern())));
        return wordsChunksByRegexes;
    }

    /**
     * Split words with occurences to separate LinkedHashMap.
     *
     * @param sortedWordsWithOccurrences the sorted words with occurrences
     * @return the {@link java.util.LinkedHashMap} with word as a key and count as a value.
     */
    private LinkedHashMap<String, Integer> splitToMapByPattern(Map<String, Integer> sortedWordsWithOccurrences,
                                                               String pattern) {
        return sortedWordsWithOccurrences.entrySet().stream()
                .filter(item -> Pattern.matches(pattern, item.getKey()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue,
                        LinkedHashMap::new));
    }
}
