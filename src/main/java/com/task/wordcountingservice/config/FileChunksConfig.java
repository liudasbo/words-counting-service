package com.task.wordcountingservice.config;

import com.task.wordcountingservice.model.FileChunk;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.Map;

@ConfigurationProperties("wordcountingservice")
@Component
public class FileChunksConfig {

    private Map<String, FileChunk> fileChunks = new LinkedHashMap<>();

    public Map<String, FileChunk> getFileChunks() {
        return fileChunks;
    }

}
