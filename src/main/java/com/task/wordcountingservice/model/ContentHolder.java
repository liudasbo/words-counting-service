package com.task.wordcountingservice.model;

import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.CopyOnWriteArrayList;

@Component
public class ContentHolder {

    @Getter
    final Map<String, Integer> wordsWithCount = new ConcurrentSkipListMap<>();

    @Getter
    final List<String> uploadedWords = new CopyOnWriteArrayList<>();

}
