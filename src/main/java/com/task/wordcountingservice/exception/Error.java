package com.task.wordcountingservice.exception;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Error {

    private String message;
    private String code;

}
