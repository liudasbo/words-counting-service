package com.task.wordcountingservice.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

import static com.task.wordcountingservice.exception.ErrorType.*;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Handle MethodArgumentNotValidException. Triggered when an object fails @Valid validation.
     *
     * @param ex      the MethodArgumentNotValidException that is thrown when @Valid validation fails
     * @param headers HttpHeaders
     * @param status  HttpStatus
     * @param request WebRequest
     * @return the Error object
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        log.error(ex.getMessage(), ex.getCause());
        return buildResponseEntity(METHOD_ARG_NOT_VALID_ERROR.message,
                METHOD_ARG_NOT_VALID_ERROR.status,
                METHOD_ARG_NOT_VALID_ERROR.code);
    }

    /**
     * Handle HttpMediaTypeNotSupportedException. Triggered when content type is not accepted.
     *
     * @param ex      HttpMediaTypeNotSupportedException
     * @param headers HttpHeaders
     * @param status  HttpStatus
     * @param request WebRequest
     * @return the Error object
     */
    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(
            HttpMediaTypeNotSupportedException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        StringBuilder builder = new StringBuilder();
        builder.append(ex.getContentType());
        builder.append(CONTENT_TYPE_NOT_SUPPORTED_ERROR.message);
        ex.getSupportedMediaTypes().forEach(t -> builder.append(t).append(", "));
        log.error(CONTENT_TYPE_NOT_SUPPORTED_ERROR.code, ex.getCause());
        return buildResponseEntity(builder.toString(), CONTENT_TYPE_NOT_SUPPORTED_ERROR.status,
                CONTENT_TYPE_NOT_SUPPORTED_ERROR.code);
    }

    /**
     * Handle MultipartFile Exception. Triggered when either file has breached file size limit
     * or no file has been attached at all
     *
     * @param request the request
     * @param ex      the ex
     * @return the response entity
     */
    @ExceptionHandler(MultipartException.class)
    public ResponseEntity<Object> handleMultipartFileException(HttpServletRequest request, Throwable ex) {
        log.error(MULTIPART_FILE_ERROR.message, ex);
        return buildResponseEntity(MULTIPART_FILE_ERROR.message, MULTIPART_FILE_ERROR.status,
                MULTIPART_FILE_ERROR.code);
    }

    /**
     * Build Error Response Entity
     *
     * @param errMessage errMessage
     * @param status HttpStatus
     * @return ResponseEntity
     */
    private ResponseEntity<Object> buildResponseEntity(String errMessage, HttpStatus status, String code) {
        Error error = new Error();
        error.setMessage(errMessage);
        error.setCode(code);
        return new ResponseEntity<>(error, status);
    }
}
