package com.task.wordcountingservice.exception;

import org.springframework.http.HttpStatus;

import static org.springframework.http.HttpStatus.*;

public enum ErrorType {

    METHOD_ARG_NOT_VALID_ERROR("Arguments not valid", BAD_REQUEST, "E1001"),
    CONTENT_TYPE_NOT_SUPPORTED_ERROR(" media type is not supported. Supported media types are ",
            UNSUPPORTED_MEDIA_TYPE, "E1002"),
    METHOD_NOT_SUPPORTED_ERROR("Method '%s' is not supported", METHOD_NOT_ALLOWED, "E1003"),
    MULTIPART_FILE_ERROR("Either no file has been attached or it has exceeded the file size limit", BAD_REQUEST,
            "E1005");


    public final String message;
    public final HttpStatus status;
    public final String code;

    ErrorType(String message, HttpStatus status, String code) {
        this.message = message;
        this.status = status;
        this.code = code;
    }
}