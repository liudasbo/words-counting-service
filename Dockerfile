FROM openjdk:8-jdk-alpine
RUN addgroup -g 1000 testuser && adduser -u 1000 -G testuser -s /bin/sh -D testuser
USER testuser
RUN mkdir -p /home/testuser
WORKDIR /home/testuser
ADD target/word-counting-service-0.0.1-SNAPSHOT.jar word-counting-service.jar
ENV wordcountingservice.filesPath=/home/testuser/
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "word-counting-service.jar"]