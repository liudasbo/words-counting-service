# Words counting service

Words counting service will accept files, process their content, count reoccurring words
and write to separate files depending on the configuration.

## Running the service locally

### Prereqs

- Java 8
- maven v3.3.1+
- Docker client (Optional)

#### Compile and run the tests

``mvn clean install``

#### Run the service

``mvn spring-boot:run ``

#### Run in docker container

- Build image from DockerFile
	
	```` docker build . ````


- Spin up container from built image
	
	```` docker run -p 8080:8080 <image Id>````



### Service configuration

Service allows configuring regex patterns, file names and file paths via external configuration:

wordcountingservice.fileChunks.<_fileName_>.regexPattern=<_regex pattern_>
wordcountingservice.filesPath=<_absolute or relative path_>

E.g. Following configuration will instruct service to put files into 4 different files
according to the regex.


````

wordcountingservice.fileChunks.firstFile.regexPattern=^[-a-gA-G].*
wordcountingservice.fileChunks.secondFile.regexPattern=^[-h-nH-N].*
wordcountingservice.fileChunks.thirdFile.regexPattern=^[-o-uO-U].*
wordcountingservice.fileChunks.fourthFile.regexPattern=^[-v-zV-Z].*

wordcountingservice.filesPath=src/main/resources/

````

## Accessing API
API can be tested via any REST client or swagger UI

* Testing via REST client


````
      curl --location --request POST 'localhost:8080/upload' --form 'files=@/Users/CleverUser/test.txt'

````


* Testing via swagger UI

	http://localhost:8080/swagger-ui.html#
